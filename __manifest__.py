# -*- coding: utf-8 -*-
# Copyright 2017 Davide Corio and Loris Tissino
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Stock 3dview',
    'summary': """
        Stock 3D View""",
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Davide Corio, Odoo Community Association (OCA), and Loris Tissino',
    'website': 'http://davidecorio.com',
    'depends': [
        'stock',
    ],
    'data': [
        'views/assets.xml',
        'views/stock_view.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'demo': [
    ],
}
